
## Vagrant machine

### First time

1. Clone the [lemp-salt](https://gitlab.com/ethanredmond/lemp-salt) repo with `git clone git@gitlab.com:ethanredmond/lemp-salt.git`
1. Run this to copy the `config.example` if you haven't set `config.sls` before: `cp ~/mnt/ebs1/git/lemp-salt/saltMount/pillars/config.example ~/mnt/ebs1/git/lemp-salt/saltMount/pillars/config.sls`
1. Edit the `config.sls` file:
    - Generate `dbRootPassword` and `dbServerPassword` with Lastpass's password generator (Alt+G), set password length to 32.
    - Set the other options in the file.
1. Run this to copy the `users.example` if you haven't set `users.sls` before: `cp ~/mnt/ebs1/git/lemp-salt/saltMount/pillars/users.example ~/mnt/ebs1/git/lemp-salt/saltMount/pillars/users.sls`
1. Edit the `users.sls` file:
    - Set your name, email, and SSH private key.
1. `cd` into `/c/mnt/ebs1/git/yourRepo`.
1. Run `vagrant destroy -f; vagrant up; vagrant ssh` to install your vagrant machine. This'll remove any previous vagrant instance that was in that directory.
1. Once it's finished installing, `init 0` and start it again.
1. Check that the time is correct with: `date "+%Y-%m-%d %H:%M:%S"`. If it isn't, fix it with: `date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"`.
1. Might also need to do `cd /mnt/ebs1/git/plannar; php composer.phar install --prefer-dist`

### Updating

1. Pull `lemp-salt`.
1. Make sure the `config.sls` is correct.
1. Run the highstate in the vagrant machine:

    ```bash
    time salt-call state.highstate | tee /tmp/saltHighstate.output
    ```
