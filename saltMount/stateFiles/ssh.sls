/root/.ssh/:
  file.directory:
    - dir_mode: 700
    - file_mode: 600
    - recurse:
      - mode

{% if pillar['envType'] == "prod" %}

/root/.ssh/id_rsa:
  file.managed:
    - source: salt://rebuildServerProd/root/.ssh/id_rsa
    - user: root
    - group: root
    - mode: 0600

/root/.ssh/id_rsa.pub:
  file.managed:
    - source: salt://rebuildServerProd/root/.ssh/id_rsa.pub
    - user: root
    - group: root
    - mode: 0600

{% else %}

set-root-password:
  cmd.run:
    - name: |
        echo root:vagrant | chpasswd

/root/.ssh/id_rsa:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - contents_pillar: user:sshPrivateKey

{% endif %}

change-authorized_keys:
  file.managed:
    - name: /root/.ssh/authorized_keys
    - source: salt://rebuildServer/root/.ssh/authorized_keys
    - user: root
    - group: root
    - mode: 0600
    - force: True
