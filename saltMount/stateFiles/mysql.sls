mysql-packages:
  pkg.installed:
    - refresh: True
    - skip_verify: True
    - names:
      - libaio1
      - libmecab2
      - libnuma1
      - psmisc
      - default-mysql-server

set-mysql-password:
  cmd.run:
    - name:
        mysqladmin --user root password '{{pillar['dbRootPassword']}}'
    - unless:
        mysql --user root --password='{{pillar['dbRootPassword']}}' --execute="SELECT 1;"
    - require:
        - service: mysql

/etc/mysql/conf.d/custom.cnf:
  file.managed:
    - source: salt://rebuildServer/etc/mysql/conf.d/custom.cnf
    - template: jinja
    - mode: 0755

maria-open-bind-address:
  cmd.run:
    - name: sed -i -e 's/\(bind-address *=\) 127\.0\.0\.1/\1 0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf

mysql-create:
  cmd.run:
    - name: |
        cat << EOF | mysql --user root --password='{{pillar['dbRootPassword']}}'
        use mysql;
        CREATE USER IF NOT EXISTS 'server'@'%' IDENTIFIED BY '{{pillar['dbServerPassword']}}';
        GRANT ALL PRIVILEGES ON * . * TO 'server'@'%';
        CREATE DATABASE IF NOT EXISTS {{pillar['gitRepoName']}};
        ALTER DATABASE {{pillar['gitRepoName']}} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        DROP USER IF EXISTS 'root'@'%';
        CREATE USER 'root'@'%' IDENTIFIED BY '{{pillar['dbRootPassword']}}';
        GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
        FLUSH PRIVILEGES;
        EOF
    - require:
      - service: mysql

mysql:
  service.running:
    - enable: True
    - watch:
        - file: /etc/mysql/conf.d/custom.cnf
