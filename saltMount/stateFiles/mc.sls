# Setup midnight commander.
install-mc:
  pkg.installed:
    - name: mc

setup-mc-conf-1:
  file.managed:
    - name: /root/.config/mc/ini
    - source: salt://rebuildServer/etc/mc-ini
    - template: jinja
    - makedirs: True
    - require:
      - pkg: install-mc

setup-mc-conf-2:
  file.managed:
    - name: /home/vagrant/.config/mc/ini
    - source: salt://rebuildServer/etc/mc-ini
    - template: jinja
    - makedirs: True
    - require:
      - pkg: install-mc
