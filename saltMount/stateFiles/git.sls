git:
  pkg.installed

gitConfigEmail:
  git.config_set:
    - name: user.email
    - value: {{pillar['user']['email']}}
    - global: True
    - require:
      - pkg: git

gitConfigName:
  git.config_set:
    - name: user.name
    - value: {{pillar['user']['fullName']}}
    - global: True
    - require:
      - pkg: git

gitConfigSafecrlf:
  git.config_set:
    - name: core.safecrlf
    - value: False
    - global: True
    - require:
      - pkg: git

gitConfigIgnorecase:
  git.config_set:
    - name: core.ignorecase
    - value: True
    - global: True
    - require:
      - pkg: git
