
redis-packages:
  pkg.installed:
    - names:
      - build-essential
      - tcl

redis-download:
  cmd.run:
    - name: |
        curl -O http://download.redis.io/redis-stable.tar.gz
        tar xzvf redis-stable.tar.gz
    - cwd: /tmp

redis-install:
  cmd.run:
    - name: |
        make
        make install
    - cwd: /tmp/redis-stable

redis-create-dir:
  file.directory:
    - name: /etc/redis

redis-copy-conf:
  file.copy:
    - name: /etc/redis/redis.conf
    - source: /tmp/redis-stable/redis.conf

redis-replace-conf-supervised:
  file.replace:
    - name: /etc/redis/redis.conf
    - backup: False
    - pattern: supervised no
    - repl: supervised systemd

redis-replace-conf-dir:
  file.replace:
    - name: /etc/redis/redis.conf
    - backup: False
    - pattern: dir ./
    - repl: dir /var/lib/redis

/etc/systemd/system/redis.service:
  file.append:
    - text: |
        [Unit]
        Description=Redis In-Memory Data Store
        After=network.target

        [Service]
        User=redis
        Group=redis
        ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
        ExecStop=/usr/local/bin/redis-cli shutdown
        Restart=always

        [Install]
        WantedBy=multi-user.target

redis-add-user:
  cmd.run:
    - name: |
        adduser --system --group --no-create-home redis

/var/lib/redis:
  file.directory:
    - user: root
    - group: root
    - mode: 777

redis-restart:
  service.running:
    - enable: True
    - reload: True
    - name: redis


