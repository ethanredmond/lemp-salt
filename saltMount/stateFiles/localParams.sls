
mkdir-getCredScripts:
  file.directory:
    - name: /getCredScripts

generate-local-params:
  file.managed:
    - name: /getCredScripts/localParams.csv
    - source: salt://rebuildServer/localParams
    - template: jinja
    - makedirs: True
