hostname-overwrite:
  file.managed:
    - name: /etc/hostname
    - source: salt://rebuildServer/etc/hostname
    - template: jinja
    - backup: .bak
  cmd.run:
    - name: hostname {{pillar['machineName']}}

hosts-file:
  file.managed:
    - name: /etc/hosts
    - source: salt://rebuildServer/etc/hosts
    - template: jinja
    - backup: .bak
