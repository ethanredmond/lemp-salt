php-packages:
  pkg.installed:
    - refresh: True
    - names:
      - git
      - php{{pillar['phpVersion']}}-cli
      - php{{pillar['phpVersion']}}-curl
      - php{{pillar['phpVersion']}}-dev
      - php{{pillar['phpVersion']}}-fpm
      - php{{pillar['phpVersion']}}-gd
      - php{{pillar['phpVersion']}}-imap
      - php{{pillar['phpVersion']}}-intl
      - php{{pillar['phpVersion']}}-mbstring
      - php{{pillar['phpVersion']}}-mysql
      - php{{pillar['phpVersion']}}-xml
      - php{{pillar['phpVersion']}}-zip
{% if pillar['envType'] == "dev" %}
      - php-xdebug
{% endif %}
    - require:
      - pkg: nginx-package

apache2*:
  pkg.purged

www-conf-replace2:
  file.replace:
    - name: /etc/php/{{pillar['phpVersion']}}/fpm/pool.d/www.conf
    - backup: False
    - pattern: .php .php3 .php4 .php5 .php7
    - repl: .php .php3 .php4 .php5 .php7\nsecurity.limit_extensions = .php .kml

# pm.max_children - Should be set to: (prod server's unused ram) / (approx amount of memory used by each PHP process, currently around 50mb)
/etc/php/{{pillar['phpVersion']}}/fpm/pool.d/www.conf:
  file.append:
    - text: |
        listen.owner    = www-data
        listen.group    = www-data
        listen.mode     = 0660
        listen          = 127.0.0.1:9000
        pm.max_children = 50

php{{pillar['phpVersion']}}-fpm-restart:
  service.running:
    - enable: True
    - reload: True
    - name: php{{pillar['phpVersion']}}-fpm
    - watch:
      - pkg: php{{pillar['phpVersion']}}-fpm

append-php-conf:
  file.append:
    - names:
      - /etc/php/{{pillar['phpVersion']}}/fpm/php.ini
      - /etc/php/{{pillar['phpVersion']}}/cli/php.ini
    - source: salt://rebuildServer/etc/php.ini-append

append-php-conf-xdebug-coverage:
  file.append:
    - name: /etc/php/{{pillar['phpVersion']}}/cli/php.ini
    - text: xdebug.mode=coverage

update-php-conf:
  file.replace:
    - name: /etc/php/{{pillar['phpVersion']}}/cli/php.ini
    - backup: False
    - pattern: = 128M
    - repl: = 192M

# On dev opcache should check file timestamps on every file usage. On prod the opcache is reset manually.
append-php-conf-opcache:
  file.append:
    - names:
      - /etc/php/{{pillar['phpVersion']}}/fpm/php.ini
      - /etc/php/{{pillar['phpVersion']}}/cli/php.ini
    - text: |
        opcache.validate_timestamps     = 1
        opcache.memory_consumption      = 48

append-php-conf-xdebug:
  file.append:
    - names:
      - /etc/php/{{pillar['phpVersion']}}/fpm/php.ini
    - source: salt://rebuildServer/etc/php.ini-xdebug

/var/log:
  file.directory:
    - mode: 0777

composer-update:
  cmd.run:
    - name: |
        ln -sf /usr/bin/php{{pillar['phpVersion']}} /etc/alternatives/php
        chmod 0755 /mnt/ebs1/git/{{pillar['gitRepoName']}}/composer.phar
        chmod 0755 /mnt/ebs1/git/{{pillar['gitRepoName']}}/vendor/
        php /mnt/ebs1/git/{{pillar['gitRepoName']}}/composer.phar install --no-interaction --prefer-dist --optimize-autoloader
    - cwd: /mnt/ebs1/git/{{pillar['gitRepoName']}}
    - require:
        - pkg: git
        - pkg: php{{pillar['phpVersion']}}-cli

bashrc-update:
  cmd.run:
    - name: sudo php /mnt/ebs1/git/{{pillar['gitRepoName']}}/deploy.phpx bashrc
    - require:
      - pkg: git
      - pkg: php{{pillar['phpVersion']}}-cli
