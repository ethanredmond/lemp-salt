
web-data-append:
  file.managed:
    - name: /etc/init/mkdir_mnt_log.conf
    - source: salt://rebuildServer/etc/mkdir_mnt_log.conf
    - makedirs: True

mkdir-nginx-log:
  file.directory:
    - name: /mnt/log/nginx
    - makedirs: True

nginx-conf-replace-log:
  file.replace:
    - name: /etc/nginx/nginx.conf
    - backup: False
    - pattern: /var/log/
    - repl: /mnt/log/

nginx-conf-replace-sites-enabled:
  file.replace:
    - name: /etc/nginx/nginx.conf
    - backup: False
    - pattern: sites-enabled/\*;
    - repl: sites-enabled/* ;\n\tinclude /mnt/ebs1/git/{{pillar['gitRepoName']}}/nginx/nginx-append.conf;\n\tinclude /mnt/ebs1/git/{{pillar['gitRepoName']}}/nginx/sites-enabled/* ;

/etc/nginx/mime.types:
  file.replace:
    - backup: False
    - pattern: application/octet-stream\t\teot;\napplication/octet-stream
    - repl: application/octet-stream\t\teot;\n\tfont/ttf\t\t\t\tttf;\n\tfont/otf\t\t\t\totf;\n\tapplication/font-woff\t\t\twoff;\napplication/octet-stream

Move-nginx-to-start-after-vagrant-has-mounted-the-nfs-share:
  file.replace:
    - name: /lib/systemd/system/nginx.service
    - backup: False
    - pattern: After=network.target\n
    - repl: After=network.target network-online.target mnt-ebs1.mount\n

# Check the nginx config with this command: nginx -c /etc/nginx/nginx.conf -t
nginx-conf-replace-geoip:
  file.replace:
    - name: /etc/nginx/nginx.conf
    - backup: False
    - pattern: http {
    - repl: http{\ngeoip_country /usr/share/GeoIP/GeoIP.dat;\n


/etc/nginx/sites-available/default:
  file.absent:
    - require:
      - pkg: php-packages

/etc/nginx/sites-enabled/default:
  file.absent:
    - require:
      - pkg: php-packages

{% if pillar['envType'] == "dev" %}

nginx:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/nginx.conf
      - pkg: nginx

{% endif %}

{% if pillar['gitRepoName'] == "pouchExamples" %}

# Self-signed cert.
dev-nginx-self-signed-ssl_certificate:
  cmd.run:
    - name: |
        apt-get install ca-certificates
        wget https://www.openssl.org/source/openssl-1.1.1g.tar.gz
        tar -zxf openssl-1.1.1g.tar.gz && cd openssl-1.1.1g
        ./config
        make
        make test
        mv /usr/bin/openssl ~/tmp
        make install
        ln -s /usr/local/bin/openssl /usr/bin/openssl
        ldconfig
        openssl req -new -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 \
        -days 730 -nodes -x509 \
        -subj "/C=GA/ST=Denial/L=Springfield/O=pouchexamples.com/CN=*.pouchexamples.com" \
        -addext "subjectAltName = DNS:*.pouchexamples.com" \
        -keyout /etc/ssl/private/selfsigned.key \
        -out /etc/ssl/certs/selfsigned.crt
{% endif %}
