# Set Bash as shell instead of dash.

bash-shell-delete:
  file.absent:
    - name: /bin/sh

bash-shell-link:
  file.symlink:
    - name: /bin/sh
    - target: /bin/bash

bash-aliases:
  file.append:
    - name: /etc/bash.bashrc
    - text: |
        restart () {
            serviceFullName="$1"
            if [ $serviceFullName == 'php' ]; then
                serviceFullName='php{{pillar['phpVersion']}}-fpm'
            fi
            service "$serviceFullName" restart
        }

        alias ll='ls -al --color=auto'
        alias psg='ps -ef|grep '
        alias "rmlo=rm /tmp/log.txt"
        alias "calo=cat /tmp/log.txt"
        alias "talo=tail /tmp/log.txt"
        alias "rp=service php{{pillar['phpVersion']}}-fpm restart"
        alias "clr=clear; clear"
        alias "rn=service nginx restart"
        alias "hr=printf '\033[41;1m%*s\033[0m\n' \"${COLUMNS:-$(tput cols)}\" '' | tr ' ' -"
        alias "_b=nano /etc/bash.bashrc"

bash-config:
  file.append:
    - name: /root/.bashrc
    - text: |
            export HISTCONTROL=ignoredups:erasedups

{% if pillar['terminalShadeCode'] == "pur" %}
# Pur.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
        PS1='${debian_chroot:+($debian_chroot)}\u\[\033[01;35m\]@\h\[\033[00m\]:\w\[\033[01;35m\]#\[\033[00m\] '
{% elif pillar['terminalShadeCode'] == "pud" %}
# Pud.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
        PS1='${debian_chroot:+($debian_chroot)}\u\[\033[0;35m\]@\h\[\033[00m\]:\w\[\033[0;35m\]#\[\033[00m\] '
{% elif pillar['terminalShadeCode'] == "bll" %}
# Bll.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
            PS1='${debian_chroot:+($debian_chroot)}\u\[\033[01;34m\]@\h\[\033[00m\]:\w\[\033[01;34m\]#\[\033[00m\] '
{% elif pillar['terminalShadeCode'] == "cyn" %}
# Cyn.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
            PS1='${debian_chroot:+($debian_chroot)}\u\[\033[01;36m\]@\h\[\033[00m\]:\w\[\033[01;36m\]#\[\033[00m\] '
{% elif pillar['terminalShadeCode'] == "red" %}
# Red.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
            PS1='${debian_chroot:+($debian_chroot)}\u\[\033[01;31m\]@\h\[\033[00m\]:\w\[\033[01;31m\]#\[\033[00m\] '
{% elif pillar['terminalShadeCode'] == "grl" %}
# Grl.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
            PS1='${debian_chroot:+($debian_chroot)}\u\[\033[01;32m\]@\h\[\033[00m\]:\w\[\033[01;32m\]#\[\033[00m\] '
{% else %}
# Default.
terminal-color:
  file.append:
    - name: /root/.bashrc
    - text: |
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
{% endif %}
