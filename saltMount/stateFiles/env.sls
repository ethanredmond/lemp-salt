
/mnt/.env:
  file.managed:
    - source: salt://rebuildServer/etc/.env
    - template: jinja
    - backup: .bak
