base:
  '*':
#    - test

    - ssh
    - pkgs
    - mc
    - env
    - buildDirectories
    - php
    - bash
    - hostname
    - nginx
    - nginxConf
    - sass
{% if pillar['hasMysql'] %}
    - mysql
{% endif %}
{% if pillar['hasRedis'] %}
    - redis
{% endif %}
    - localParams
