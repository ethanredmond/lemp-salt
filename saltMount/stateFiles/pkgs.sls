
{% if 1 == salt['cmd.retcode']('test -f /opt/onlyRunOnFirstInstall') %}
# Fixes this error:
#     The following signatures couldn't be verified because the public key is not available: NO_PUBKEY # Allow non-contraction.
#     E: The repository 'http://repo.mysql.com/apt/debian buster InRelease' is not signed. # Allow non-contraction.
fix-mysql-no-pubkey:
  cmd.run:
    - name: apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 467B942D3A79BD29
{% endif %}

temp-add-repos:
  cmd.run:
    - name: |
        iptables -F
        apt-get update
        apt-get install -y apt-transport-https
        wget -O - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
        wget -O - https://repo.mysql.com/RPM-GPG-KEY-mysql | sudo apt-key add -
        echo "deb https://packages.sury.org/php/ {{pillar['linuxVersionCode']}} main" > /etc/apt/sources.list.d/php.list
        echo "deb http://repo.mysql.com/apt/debian/ {{pillar['linuxVersionCode']}} mysql-8.0" > /etc/apt/sources.list.d/mysql.list
        apt-get update

base-packages:
  pkg.installed:
    - refresh: True
    - names:
      - htop
      - software-properties-common
      - curl
      - rsync

npm:
  cmd.run:
    - name: curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -

nodejs:
  cmd.run:
    - name: apt-get install -y nodejs

stylelint:
  cmd.run:
    - name: npm install -g stylelint
