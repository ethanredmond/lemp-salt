mkdirs/chmod/chown-filesCache-emailQueue:
  file.directory:
    - names:
      - /mnt/ebs1/filesCache/emailQueue
      - /mnt/ebs1/filesCache/emailQueue/failed
      - /mnt/ebs1/filesCache/smsQueue
      - /mnt/ebs1/filesCache/smsQueue/failed
    - makedirs: True
{% if pillar['envType'] == "prod" %}
    - user: www-data
    - group: www-data
    - dir_mode: 0755
    - file_mode: 0755
    - recurse:
      - mode
      - user
      - group
{% endif %}

mkdirs/chmod/chown-filesCache:
  file.directory:
    - names:
      - /mnt/ebs1/filesCache/imageResize
    - makedirs: True
{% if pillar['envType'] == "prod" %}
    - user: www-data
    - group: www-data
    - dir_mode: 0700
    - file_mode: 0700
    - recurse:
      - user
      - group
      - mode
{% endif %}

mkdir/chown-git:
  file.directory:
    - name: /mnt/ebs1/git
    - makedirs: True
{% if pillar['envType'] == "prod" %}
    - user: www-data
    - group: www-data
    - recurse:
      - user
      - group
{% endif %}


# These are done on restart by saltMount/stateFiles/rebuildServer/etc/mkdir_mnt_log.conf
mkdir/chmod/chown-log:
  file.directory:
    - names:
      - /mnt/log
      - /mnt/tmp
      - /mnt/log/email
    - makedirs: True
    - user: www-data
    - group: www-data
    - dir_mode: 0777
    - file_mode: 0777
    - recurse:
      - user
      - group
      - mode
