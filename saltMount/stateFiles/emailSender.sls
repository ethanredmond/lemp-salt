php{{pillar['phpVersion']}}-cli:
  pkg.installed

emailSenderService:
  file.managed:
    - name: /lib/systemd/system/emailSender.service
    - source: salt://rebuildServer/lib/systemd/system/emailSender.service
    - template: jinja
    - mode: 0755
    - require:
      - pkg: php{{pillar['phpVersion']}}-cli


{% if pillar['envType'] != "prod" %}
emailSender-conf-replace:
  file.replace:
    - name: /lib/systemd/system/emailSender.service
    - backup: False
    - pattern: =redis-server.service
    - repl: =mysql.service redis-server.service
{% endif %}

emailSender:
  service.running:
    - enable: True
    - full_restart: True
    - order: last
